# book

Web application on https://book.p3ll3.eu with various book editing utilities: names index and OPAC search.

## Description
### Names index:
Match PDF file with a list of words.

This tool search the first word (delimited by comma or space) of each row appending the relative page numbers (matches count optional).

All searches are logged (both files and words).

Matching methods:
- _pdfgrep_ (default): slower but more reliable
- _pdftotext_ and _strpos_: much faster but miss some occurrences


### OPAC search:
Search book info quering OPAC SBN JSON API.
Fetch API using _Curl_


## Requirements
### Server:
- PHP
- pdfgrep
- pdftotext

### Client:
- Browser supporting ES6


