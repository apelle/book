<?php
  //Return formatted matches: Surname, Name pageX [, pageN]
  function formatMatches($match, $words, $mc) {
    $out = "";
    
    //Loop matched words
    foreach($words as $i => $w) {
      
      //Check if word has matches
      if(isset($match[$i])) {
        $f = "";
        
        //Loop matches
        foreach($match[$i] as $p => $c) {
          //Write page number and optionally (if $mc and more than one) write the number of occurrences in page
          $f .= $p . ($mc && $c ? " ({$c}), " : ", ");
        }
        
        //Write in out var, remove trailing space and comma, add EOL
        $out .= "{$w} " . substr($f, 0, -2) . "\n"; 
        
      } else {
        //Write in out var, remove trailing space, add EOL
        $out .= "{$w}\n";
      }
    }
    
    return $out;
  }
  
  
  //Write search info to log and save uploaded file and words
  function logSearch($pdf, $opdf, $text) {
    //Build base path
    $filepath = $_SERVER["DOCUMENT_ROOT"] . "/files";
    //Build filename (time)
    $filetime = strftime("%Y%m%d%H%M%S");
    //Build filenames
    $filepdf =  $filetime . ".pdf";
    $filetext = $filetime . ".txt";
    //Copy uploaded file
    //$filepdf = move_uploaded_file($pdf, "{$filepath}/{$filepdf}") ? $filepdf : "ERR_PDF";
    $filepdf = (is_uploaded_file($pdf) && copy($pdf, "{$filepath}/{$filepdf}")) ? $filepdf : "ERR_PDF";
    //Write words to file
    $filetext = file_put_contents("{$filepath}/{$filetext}", $text) ? $filetext : "ERR_TXT";
    
    //Build and write log string to file
    file_put_contents("{$filepath}/search_log.log", strftime("%d/%b/%Y:%H:%M:%S %z") . "; " . $_SERVER["REMOTE_ADDR"] . "; [" . $_SERVER["HTTP_USER_AGENT"] . "]; {$filepdf} ({$opdf}); {$filetext}\n", FILE_APPEND);
  }
  
  
  //Match words in pdf using pdftotext and strpos
  function pdfMatchPTT($file, $words, $separator) {
    //Extract text from pdf and split pages
    $book = explode("\f", shell_exec("pdftotext {$file} -"));
    
    $match = [];
    //Loop words
    foreach($words as $i => $w) {
      //Use only the leading part (before the separator: comma/space) of the word
      $w = explode($separator, $w)[0];
      
      //Loop pages
      foreach($book as $p => $b) {
        $j = 0; //Match counter
        $o = 0; //Offset
        
        //Loop until no match is found
        while(($pos = strpos($b, $w, $o)) !== FALSE) {
          //Update offset and match counter
          $o = $pos + 1;
          $j++;
        }
        
        //Check for matches
        if($j) {
          //Write match
          $match[$i][$p+1] = $j;
        }
      }
    }
    
    return $match;
  }
  
  
  //Match words in pdf using pdfgrep
  function pdfMatchPGR($file, $words, $separator) {
    $match = [];
    
    //Loop words
    foreach($words as $i => $w) {
      //Use only the leading part (before the separator: comma/space) of the word
      $w = explode($separator, $w)[0];
      
      //Require pdfgrep
      //-p option: print only page number and relative matches count
      //-h option: do not print filename
      //--cache option: reached ~25x on 200+ pages file
      $grep = array_filter(explode("\n", shell_exec("pdfgrep -ph --cache '{$w}' {$file}")));
      
      //Loop matches and add to out
      foreach($grep as $m) {
        //Decode pdfgrep result
        $m = explode(":", $m);
        //Add to match
        $match[$i][$m[0]] = intval($m[1]);
      }
    }
    
    return $match;
  }
  
  
  //JSON header
  header('Content-type:application/json;charset=utf-8');
  
  //Get first uploaded file
  $f = reset($_FILES);
  
  //Check if no word is provided and if there are upload errors
  if(!isset($_POST["words"]) || $f["error"] !== UPLOAD_ERR_OK) {
    //Return error
    echo json_encode(array("result" => -1, "error" => "FILE UPLOAD ERROR"));
    exit;
  }
  
  //Get words
  $words = filter_input(INPUT_POST, "words", FILTER_SANITIZE_STRING);
  //Get word separator: 
  $separator = filter_input(INPUT_POST, "separator", FILTER_SANITIZE_STRING);
  //Get mc (Match Count) flag: print number of matches in a page
  $mc = filter_input(INPUT_POST, "mc", FILTER_VALIDATE_BOOLEAN) ?? false;
  //Get mode: select algorithm for searching (default pdfgrep)
  $mode = filter_input(INPUT_POST, "mode", FILTER_SANITIZE_STRING) ?? "pdfgrep";
  
  //Get file data
  $fname = filter_var($f["name"], FILTER_SANITIZE_STRING);
  $ftype = filter_var($f["type"], FILTER_SANITIZE_STRING);
  $ftmp = filter_var($f["tmp_name"], FILTER_SANITIZE_STRING);
  
  //Check if file is not a PDF
  if($ftype != 'application/pdf') {
    //Return error
    echo json_encode(array("result" => -1, "error" => "FILE UPLOAD ERR"));
    exit;
  }
  
  //Log search
  logSearch($ftmp, $fname, $words);
  
  //Split words in array
  $words = array_filter(preg_split("/\r\n|\n|\r/", $words));
  
  //Select matching mode
  if($mode == "pdftotext") {
    $match = pdfMatchPTT($ftmp, $words, $separator);
  } else {
    $match = pdfMatchPGR($ftmp, $words, $separator);
  }
  
  //Get formatted matches
  $machF = formatMatches($match, $words, $mc);
  
  //Return results
  echo json_encode(array("result" => 0, "match" => $match, "words" => $words, "matchF" => $machF));//, "book" => $book
?>