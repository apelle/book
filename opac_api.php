<?php
  header('Content-type:application/json;charset=utf-8');
  
  $query = isset($_GET["q"]) ? urlencode(filter_input(INPUT_GET, "q", FILTER_SANITIZE_STRING)) : null;
  $bid = isset($_GET["bid"]) ? urlencode(filter_input(INPUT_GET, "bid", FILTER_SANITIZE_STRING)) : null;
  
  if($query) {
    $url = "https://opac.sbn.it/opacmobilegw/search.json?any={$query}&type=0&start=0&rows=10";
  } elseif($bid) {
    $url = "http://opac.sbn.it/opacmobilegw/full.json?bid={$bid}";
  } else {
    echo json_encode(array("result" => -1, "error" => "NO QUERY"));
    exit;
  }
  
  function parseTitle($title) {
    preg_match("/(.*)( : )?(.*)( \/ )?(.*)/i", $title, $t);//(.*) : (.*), (.*)
    return array(
      "titolo" => $t[1] ?? null,
      "sottotitolo" => $t[3] ?? null,
      "autore" => $t[5] ?? null
    );
  }
  
  function parsePubb($pubb) {
    preg_match("/\[(.*) : (.*), (.*)(\?\!)?/i", $pubb, $p);
    return array(
      "luogo" => $p[1] ?? null,
      "editore" => $p[2] ?? null,
      "anno" => $p[3] ?? null
    );
  }
  
  
  $curl = curl_init();
  curl_setopt_array($curl, array(
    CURLOPT_URL => ($url),
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_TIMEOUT => 30,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => "GET",
    CURLOPT_HTTPHEADER => array(
      "cache-control: no-cache"
    ),
  ));
  
  $data = json_decode(curl_exec($curl), true);
  curl_close($curl);
  
  if($query) {
    $books = array();
    if($data["numFound"] > 0) {
      foreach($data["briefRecords"] as $d) {
        $b["bid"] = $d["codiceIdentificativo"];
        $b["isbn"] = $d["isbn"] ?? null;
        $b["titolo"] = $d["titolo"] ?? null;
        $b["autorePrincipale"] = $d["autorePrincipale"] ?? null;
        $b["editore"] = $d["editore"] ?? null;
        //$b = array_merge($b, parseTitle($d["titolo"]), parsePubb($d["pubblicazione"]));
        //$b["copertina"] = $d["copertina"] ?? null;
        $books[] = $b;
      }
      
      echo json_encode(array("count" => $data["numFound"], "books" => $books));
    } else {
      //echo json_encode(array("count" => 0));
    }
  }
?>